# Self-hosted K8s Apps
__Note:__ Usage is not recommended just yet. There may be some major changes.

## Description
A basic Kubernetes configuration for self-hosting Nextcloud, Vaultwarden, and a static HTML website.

Traefik is used to handle internally routing to the applications, based on subdomains.

Currently, this has only been tested on K3s. I chose K3s because it is light and well-suited for a single-node setup.

## Requirements
* A Linux-based operating system.
* Kubernetes (The lighter K3s is sufficient.)
* Helm (DO NOT install if using K3s!)
* You need to have a domain name and subdomains set up. (You can modify the configuration files to work for your local network, but that's not the goal for this project.)

## Installation
Clone the repository and enter it.
```sh
cd ~/
git clone https://gitlab.com/Blazin64/k8s-apps.git
cd k8s-apps
```

Create the data directory.
```sh
mkdir /k8s-data
```

Copy an example webpage and an nginx config file for nextcloud
```sh
cp -r html nginx.conf /k8s-data/
```

Edit `yml/config.env`.
* In case it is deleted, keep a copy in a safe place.
* You should modify most of the variables in this file.
* Optionally, change `SIGNUPS_ALLOWED`. See [Disable registration of new users](https://github.com/dani-garcia/vaultwarden/wiki/Disable-registration-of-new-users) for details.
* __DO NOT__ change `ACME_STAGING`. Leave it set to `"true"`. This will be addressed at the end of the installation guide.

Generate the Kubernetes configuration files.
```sh
cd yml
./gen.sh
```

(If using Kubernetes + Helm) Install Traefik and cert-manager.
```sh
helm repo add traefik https://traefik.github.io/charts
helm install traefik traefik/traefik
kubectl apply -f https://github.com/cert-manager/cert-manager/releases/download/v1.14.2/cert-manager.yaml
```

(If using K3s) Install Traefik and cert-manager.
```sh
kubectl apply -f k3s-traefik.yml
kubectl apply -f https://github.com/cert-manager/cert-manager/releases/download/v1.14.2/cert-manager.yaml
```


Apply the YAML files. This includes some dependencies from Traefik's GitHub repository.
```sh
kubectl apply -f ingress.yml -f nginx.yml -f vaultwarden.yml -f nextcloud.yml
```

Check the status of Kubernetes.
```sh
kubectl get all
```

NOTE: If you see `pending` under `EXTERNAL-IP`, you may need to manually assign your public IP address.
```sh
kubectl patch svc traefik -p '{"spec":{"externalIPs":["YOUR IP ADDRESS HERE"]}}'
```

You may notice that your browser is complaining about the HTTPS certificate. This is because the Let's Encrypt staging certificate is used by default. Once you have verified that everything else works correctly, you need to make one last modification.

Set `ACME_STAGING="false"` in your `config.env` file.

Generate the `ingress.yml` files again to reflect the change, then apply it.
```sh
./gen.sh
kubectl apply -f ingress.yml
```

Once that last change is applied, restart your server.

Check one more time to verify everything (including HTTPS) works.

Enjoy!

## License
GPLv3
