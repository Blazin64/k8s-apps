#!/usr/bin/env sh

# Get environment variables
set -a
. ./config.env

if [ "$ACME_STAGING" = "false" ]
then
  export ACME_RESOLVER="https://acme-v02.api.letsencrypt.org/directory"
else
  export ACME_RESOLVER="https://acme-staging-v02.api.letsencrypt.org/directory"
fi

set +a

# Generate .yml files for Kubernetes
cd base

for YML in *.yml
do
  envsubst < $YML > ../$YML
done
